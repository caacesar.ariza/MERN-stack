import React from "react";
import { Container,FlatList, Text, View} from "native-base";

export const SearchedProduct =(prorps) =>{
    const {productsFiltered} = prorps
    return (
        <Container>
            {
                productsFiltered.length > 0 ? (
                    productsFiltered.map((item)=>{
                        <FlatList
                            //onPress={navigation}
                            key={item._id}
                            avatar
                        >
                            <VStack  >
                                <Image 
                                    source ={{uri:item.image}}
                                />
                            </VStack >
                            <Body>
                                <Text>{item.name}</Text>
                                <Text note>{item.description}</Text>
                            </Body>
                        </FlatList>
                    })
                ):(
                    <View>
                        <Text>
                            No products Match the selected criteria
                        </Text>
                    </View>
                )
            }
        </Container>
    )
}