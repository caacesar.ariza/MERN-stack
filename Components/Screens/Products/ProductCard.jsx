import React from "react";
import style from '../../../Styles/Style';
import { Button, Image, Text, View } from "react-native";

export const ProductCard = (porps) => {

    const { name, price, image, countInStock } = porps
    return (
        <View style={style.container}>
            <Image
                style={style.image}
                resizeMode="contain"
                source={{ uri: image ? image : 'https://cdn.pixabay.com/photo/2012/04/01/17/29/box-23649_960_720.png' }}
            />
            <View style={style.card} />
            <Text style={style.title}>
                {name.length > 15 ? name.substring(0, 15 - 3) + '...' : name}
            </Text>
            <Text style={style.price}>${price}</Text>
            {
                countInStock > 0 ? (
                    <View style={{ marginBottom: 60 }}>
                        <Button title={'Add'} className={style.btn} color={'green'}></Button>
                    </View>
                ) :
                    <Text style={{ marginTop: 60 }} >Currently Unavailable</Text>
            }
        </View>
    )
}