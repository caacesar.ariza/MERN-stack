import React, { useEffect, useState } from "react"
import { View, Text, FlatList } from "react-native"
import ProductList from "./ProductList"
import { Container, Icon, Input, Stack } from "native-base"
import { Ionicons, FontAwesome } from '@expo/vector-icons';
import { SearchedProduct } from "./SearchedProduct";

const data = require('../../../assets/data/Products.json')

const ProductContainer = () => {

    const [prodcts, setProducts] = useState([])
    const [productsFiltered, SetProductsFiltered] = useState()
    const [focus, setFocus] = useState()
    useEffect(() => {
        setProducts(data)
        SetProductsFiltered(data)
        setFocus(false)
        return () => {
            setProducts([])
            SetProductsFiltered([])
            setFocus()
        }
    })

    const searchProduct = (text) => {
        SetProductsFiltered(
            prodcts.filter((i) => i.name.toLowerCase().includes(text.toLowerCase()))
        )
    }

    const openList = () => {
        setFocus(true)
    }

    const onBluer = () => {
        setFocus(false)
    }

    return (
        <Container>
            <View>
                <Stack  space={2} alignContent={"center"}>

                    <Icon
                        as={Ionicons}
                        name="ios-search"
                    />
                    <Input
                        placeholder="Search"
                        onFocus={openList}
                        onChangeText={(text) => searchProduct(text)}
                    />
                    {focus==true ? (
                        <SearchedProduct
                            productsFiltered={productsFiltered}
                        />
                    ):(
                        <View></View>
                    )}
                </Stack>
            </View>


            <View>
                <Text>Product Container</Text>
                <View style={{ marginTop: 100 }}>
                    <FlatList
                        numColumns={2}
                        data={prodcts}
                        renderItem={({ item }) =>
                            <ProductList
                                key={item.id}
                                item={item}
                            />
                        }
                        keyExtractor={item => item.name}
                    />
                </View>
            </View>
        </Container>
    )
}
export default ProductContainer