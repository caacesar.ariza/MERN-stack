import React from "react";
import style from '../../../Styles/Style'
import { TouchableOpacity, View } from "react-native";
import { ProductCard } from "./ProductCard";

const ProductList = (porps) =>{
    const {item} = porps
    return(
        <TouchableOpacity style={style.touchHableOpacity} >
            <View style={style.touchHableOpacityView}>
                <ProductCard {...item}/>
            </View>
        </TouchableOpacity>
    )
}

export default ProductList