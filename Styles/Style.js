import React from "react"
import { Dimensions, StyleSheet } from "react-native"

let { width } = Dimensions.get('window')

const style = StyleSheet.create({
    touchHableOpacity: {
        width: '50%'
    },
    touchHableOpacityView: {
        width: width / 2,
        backgroundColor: 'gainsboro'
    },
    container: {
        width: width / 2 - 20,
        height: width / 1.7,
        padding: 10,
        borderRadius: 10,
        marginTop: 55,
        marginBottom: 5,
        marginLeft: 10,
        alignItems: 'center',
        elevation: 8,
        backgroundColor: '#fff'
    },
    image: {
        width: width / 2 - 20 - 10,
        height: width / 2 - 20 - 30,
        backgroundColor:'transparent',
        position:'absolute',
        top:-45
    },
    card:{
        marginBottom: 10,
        height :width/2 -20-90,
        backgroundColor:'transparent',
        width:width/2-20-10
    },
    title:{
        fontWeight:'bold',
        fontSize:14,
        textAlign:"center"
    },
    price:{
        fontSize:20,
        color:'orange',
        marginTop:10

    },
    btn:{
        backgroundColor:'green',
        borderRadius:3
    },
    header:{
        width:"100%",
        flexDirection:"row",
        alignContent:'center',
        justifyContent:'center',
        padding:20,
        marginTop:8

    }
})


export default style